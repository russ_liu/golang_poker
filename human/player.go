package human

import (
	"fmt"
	"golangPoker/libs"
	"golangPoker/poker"
	"sort"
)

// Player 可依照需求自行增加struct內資料或function(已經有的一定要用)
type Player struct {
	name  string
	poker poker.PokerBox
	score int
}

func CreatePlayer(name string) *Player {
	return &Player{name: name}
}

func (p *Player) GetName() string {
	return p.name
}

func (p *Player) GetPoker() *poker.PokerBox {
	return &p.poker
}

func (p *Player) GetScore() int {
	return p.score
}
func (p *Player) PlusScore() {
	p.score++
}

//玩家取牌
func (p *Player) RevCards(cards []int) {
	recvCards := make(map[int][]int)
	for _, card := range cards {
		suit, point := poker.DecodeCard(card)
		if _, isExist := recvCards[point]; !isExist {
			recvCards[point] = make([]int, 0)
		}
		recvCards[point] = append(recvCards[point], suit)
	}
	p.poker.SetCards(recvCards)
}

//玩家出牌
func (p *Player) PlayCards() []int {
	var dealCards []int
	dealCards = p.findAceStraight()
	if dealCards != nil {
		fmt.Printf("\tAceStraight:%v\n", poker.FormatCards(dealCards))
		return dealCards
	}
	dealCards = p.findStraight()
	if dealCards != nil {
		fmt.Printf("\tStraight:%v\n", poker.FormatCards(dealCards))
		return dealCards
	}
	dealCards = p.findPair()
	if dealCards != nil {
		fmt.Printf("\tPair:%v\n", poker.FormatCards(dealCards))
		return dealCards
	}
	dealCards = p.findSingle()
	fmt.Printf("\tSingle:%v\n", poker.FormatCards(dealCards))
	return dealCards
}

func (p *Player) findStraight() []int {
	points := libs.GetKeys(p.poker.GetCards())
	sort.Ints(points)
	start, end, count := points[0], 0, 1
	for ix := 0; ix < len(points)-1; ix++ {
		count++
		end = points[ix+1]
		if (end - points[ix]) != 1 {
			start = end
			count = 1
		}
		if count == 5 {
			break
		}
	}
	if count != 5 {
		return nil
	}

	idx := 0
	dealCards := make([]int, 5)
	for point := start; point <= end; point++ {
		suit := p.poker.GetCards()[point][0]
		dealCards[idx] = poker.EncodeCard(suit, point)
		idx++
	}
	p.poker.RemoveCards(dealCards)
	return dealCards
}

func (p *Player) findAceStraight() []int {
	points := libs.GetKeys(p.poker.GetCards())
	if !(libs.IsExist(points, 10) && libs.IsExist(points, poker.JACK) && libs.IsExist(points, poker.QUEEN) &&
		libs.IsExist(points, poker.KING) && libs.IsExist(points, poker.ACE)) {
		return nil
	}
	bigStraight := []int{10, poker.JACK, poker.QUEEN, poker.KING, poker.ACE}
	dealCards := make([]int, 5)
	for ix := 0; ix < len(bigStraight); ix++ {
		point := bigStraight[ix]
		suit := p.poker.GetCards()[point][0]
		dealCards[ix] = poker.EncodeCard(suit, point)
	}
	p.poker.RemoveCards(dealCards)
	return dealCards
}

func (p *Player) findPair() []int {
	for point, suits := range p.poker.GetCards() {
		if len(suits) >= 2 {
			poppedSuits := suits[:2]
			dealCards := []int{
				poker.EncodeCard(poppedSuits[0], point),
				poker.EncodeCard(poppedSuits[1], point),
			}
			p.poker.RemoveCards(dealCards)
			return dealCards
		}
	}
	return nil
}

func (p *Player) findSingle() []int {
	point := libs.GetKeys(p.poker.GetCards())[0]
	suit := p.poker.GetCards()[point][0]
	dealCards := []int{poker.EncodeCard(suit, point)}
	p.poker.RemoveCards(dealCards)
	return dealCards
}

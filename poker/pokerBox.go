package poker

import (
	"golangPoker/libs"
	"math/rand"
	"time"
)

// PokerBox 可依照需求自行增加struct內資料或function(已經有的一定要用)
type PokerBox struct {
	cards       map[int][]int //map[point]suit
	encodeCards []int
}

func (p *PokerBox) GetCards() map[int][]int {
	return p.cards
}
func (p *PokerBox) SetCards(cards map[int][]int) {
	p.cards = cards
}

func (p *PokerBox) GetEncodeCards() []int {
	return p.encodeCards
}

// 放入指定牌組數量的撲克進PokerBox ex: numOfDeck=1, 放入52張(一整副牌)
func (p *PokerBox) NewCards(numOfDeck int) {
	initSuits := make([]int, 0)
	for i := 0; i < numOfDeck; i++ {
		initSuits = append(initSuits, SPADES, HEARTS, SQUARE, PLUME)
	}
	p.cards = map[int][]int{
		1:  make([]int, numOfDeck*4),
		2:  make([]int, numOfDeck*4),
		3:  make([]int, numOfDeck*4),
		4:  make([]int, numOfDeck*4),
		5:  make([]int, numOfDeck*4),
		6:  make([]int, numOfDeck*4),
		7:  make([]int, numOfDeck*4),
		8:  make([]int, numOfDeck*4),
		9:  make([]int, numOfDeck*4),
		10: make([]int, numOfDeck*4),
		11: make([]int, numOfDeck*4),
		12: make([]int, numOfDeck*4),
		13: make([]int, numOfDeck*4),
	}
	p.encodeCards = make([]int, 0)
	for point, suits := range p.cards {
		copy(p.cards[point], initSuits)
		for _, suit := range suits {
			p.encodeCards = append(p.encodeCards, EncodeCard(suit, point))
		}
	}
}

//洗牌
func (p *PokerBox) Shuffle() {
	rand.Seed(time.Now().UnixNano())
	rand.Shuffle(len(p.encodeCards), func(i, j int) {
		p.encodeCards[i], p.encodeCards[j] = p.encodeCards[j], p.encodeCards[i]
	})
}

//剩下幾張牌
func (p *PokerBox) GetLength() int {
	rtnLen := 0
	for _, suit := range p.cards {
		rtnLen += len(suit)
	}
	return rtnLen
}

//發出指定數量的牌(已發出的牌不存在PokerBox)
func (p *PokerBox) DealCards(num int) []int {
	rtn := p.encodeCards[:num]
	p.encodeCards = p.encodeCards[num:]
	p.RemoveCards(rtn)
	return rtn
}

func (p *PokerBox) RemoveCards(encodeCards []int) {
	for _, encodeCard := range encodeCards {
		suit, point := DecodeCard(encodeCard)
		idx := libs.IndexOf(p.cards[point], suit)
		p.cards[point] = append(p.cards[point][:idx], p.cards[point][idx+1:]...)
		if len(p.cards[point]) == 0 {
			delete(p.cards, point)
		}
	}
}

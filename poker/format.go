package poker

type DisplayCard struct {
	suit  string
	point string
}

// Higher 4 bits as suit, Lower 4 bits as point
func EncodeCard(suit, point int) int {
	return suit<<4 + point
}

// Higher 4 bits as suit, Lower 4 bits as point
func DecodeCard(encodeCard int) (int, int) {
	suit := encodeCard >> 4
	point := 0x000F & encodeCard
	return suit, point
}

func FormatCards(intCards []int) []DisplayCard {
	rtn := make([]DisplayCard, len(intCards))
	for idx, v := range intCards {
		intSuit, point := DecodeCard(v)
		rtn[idx] = DisplayCard{suit: strSuitMap[intSuit], point: strPointMap[point]}
	}
	return rtn
}

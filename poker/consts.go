package poker

const CardNum = 52
const (
	SPADES = 1
	HEARTS = 2
	SQUARE = 3
	PLUME  = 4
)
const (
	ACE   = 1
	JACK  = 11
	QUEEN = 12
	KING  = 13
)

var strSuitMap = map[int]string{
	SPADES: "\033[30;43;1mSPADES\033[0m",
	HEARTS: "\033[31;43;1mHEARTS\033[0m",
	SQUARE: "\033[31;43;1mSQUARE\033[0m",
	PLUME:  "\033[30;43;1mPLUME\033[0m",
}

var strPointMap = map[int]string{
	ACE:   "\033[94;1mACE\033[0m",
	2:     "\033[1m2\033[0m",
	3:     "\033[1m3\033[0m",
	4:     "\033[1m4\033[0m",
	5:     "\033[1m5\033[0m",
	6:     "\033[1m6\033[0m",
	7:     "\033[1m7\033[0m",
	8:     "\033[1m8\033[0m",
	9:     "\033[1m9\033[0m",
	10:    "\033[1m10\033[0m",
	JACK:  "\033[94;1mJACK\033[0m",
	QUEEN: "\033[94;1mQUEEN\033[0m",
	KING:  "\033[94;1mKING\033[0m",
}

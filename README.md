# Golang Poker

根據遊戲規則，用**Stage Pattern**來設計遊戲的流程的練習。

## 遊戲規則
1. 將一整副牌放入PokerBox中
2. 洗牌
3. 平均發給4位player(A,B,C,D)
4. 由A開始輪流依照手中的牌出牌(優先出牌順序: 5張順->對子->單張),
   Ex: 第一輪: A沒有順,有對子,出對子,  B有順,出順 C有順,出順  D沒有順,有對子,出對子
      第二輪: A沒有順,沒有對子,出單張 B有順,出順 C沒有順,有對子,出對子 D.......
  誰先出完牌得一分, 進行下一局(返回1.)
5. 先贏的3分的為贏家
6. 印出各player比數

## 狀態設計

根據上述遊戲規則切割出4種狀態
- Prepare State: 準備階段
- Deal Card State: 發牌階段
- Game Round State: 遊戲階段
- Settlement State: 結算階段

### 狀態圖
```plantuml
scale 700 width
hide empty description
[*] -> PrepareState
PrepareState -down-> DealCardState
PrepareState: Players enter the game.

DealCardState -down-> GameRoundState: Start game
DealCardState: 1. Shuffles cards.
DealCardState: 2. Deals cards to players .

GameRoundState -> DealCardState: No player gets GoalScore scores
GameRoundState -right-> SettlementState: One of Player gets GoalScore scores
GameRoundState: 1. Players play cards in sequence.
GameRoundState: 2. The player who has no cards gets 1 score
GameRoundState: and ends the round.

SettlementState -down-> [*]
SettlementState: List all player scores.
```

### Class Diagram
```plantuml
class Context {
	+ numOfDeck :int
	+ goalScore :int
	- currentState :IState
	- mainPokerBox :PokerBox
	- winner :Player
	- players :Player[]
	- roundNum :int
	+ Launch() :void
}

note left of Context::"Launch()"
	Keep executing
	1. currentState.preHandle()
	2. currentState.handle()
	3. currentState.afterHandle()
	until there is no next state.
end note

interface IState {
	preHandle() :void
	handle() :void
	afterHandle() :void
}

note right of IState::"afterHandle()"
	Make context switch to the next state.
end note

class PrepareState extends IState {
	- context : Context
}
class DealCardState extends IState {
	- context : Context
}
class GameRoundState extends IState {
	- context : Context
}
class SettlementState extends IState {
	- context : Context
}
```

## Run Application

- Using "Go" to execute app.
```bash
go run main.go
```

- If "Go" is not installed, you can execute app with docker.
```bash
$ docker build -t golang_poker . --no-cache
$ docker run --name poker golang_poker
$ docker rm poker
$ docker rmi golang_poker
```

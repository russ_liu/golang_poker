package libs

//Helpers
func GetKeys(m map[int][]int) []int {
	keys := make([]int, len(m))
	i := 0
	for k := range m {
		keys[i] = k
		i++
	}
	return keys
}

func IndexOf(data []int, element int) int {
	for k, v := range data {
		if element == v {
			return k
		}
	}
	return -1
}
func IsExist(data []int, element int) bool {
	return IndexOf(data, element) != -1
}

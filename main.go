package main

import (
	"fmt"
	"golangPoker/stateMachine"
	"time"
)

func main() {
	start := time.Now()

	stateMachine.CreateContext(1, 3).Launch()

	fmt.Printf("execution time: %s\n", time.Since(start))
}

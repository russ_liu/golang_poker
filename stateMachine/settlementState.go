package stateMachine

import "fmt"

type SettlementState struct {
	context *Context
}

func (ss *SettlementState) preHandle() {
	fmt.Printf("\n\n\033[94;1;4mSettlement State...\033[0m\n")
}

func (ss *SettlementState) handle() {
	fmt.Printf("\033[37;101mThe winner is %s!!!!\033[0m\n", ss.context.winner.GetName())
	fmt.Printf("Total round:%d\n", ss.context.roundNum)
	fmt.Printf("The winner records:%v\n", ss.context.winnerRecords)
	fmt.Println("The score list:")
	for _, player := range ss.context.players {
		fmt.Printf("%s:      %d\n", player.GetName(), player.GetScore())
	}
}

func (ss *SettlementState) afterHandle() {
	fmt.Println("\033[94;1;4mEnd Game.\033[0m")
	//Next: Nil
	ss.context.currentState = nil
}

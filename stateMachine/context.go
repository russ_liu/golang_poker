package stateMachine

import (
	"golangPoker/human"
	"golangPoker/poker"
)

type Context struct {
	numOfDeck     int
	goalScore     int
	currentState  IState
	mainPokerBox  *poker.PokerBox
	winner        *human.Player
	players       []*human.Player
	winnerRecords []string
	roundNum      int
}

func (cxt *Context) Launch() {
	for cxt.currentState != nil {
		cxt.currentState.preHandle()
		cxt.currentState.handle()
		cxt.currentState.afterHandle()
	}
}

func CreateContext(numOfDeck, goalScore int) *Context {
	cxt := new(Context)
	cxt.numOfDeck = numOfDeck
	cxt.goalScore = goalScore
	cxt.currentState = &PrepareState{context: cxt}
	cxt.winnerRecords = make([]string, 0)
	return cxt
}

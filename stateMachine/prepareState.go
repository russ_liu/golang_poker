package stateMachine

import (
	"fmt"
	"golangPoker/human"
	"golangPoker/poker"
)

type PrepareState struct {
	context *Context
}

func (ps *PrepareState) preHandle() {
	fmt.Printf("\n\n\033[94;1;4mPrepare State...\033[0m\n")
}

func (ps *PrepareState) handle() {
	ps.context.players = []*human.Player{
		human.CreatePlayer("PlayerA"),
		human.CreatePlayer("PlayerB"),
		human.CreatePlayer("PlayerC"),
		human.CreatePlayer("PlayerD"),
	}
	for _, player := range ps.context.players {
		fmt.Printf("%s sits at the desk...\n", player.GetName())
	}
	ps.context.mainPokerBox = &poker.PokerBox{}
}

func (ps *PrepareState) afterHandle() {
	fmt.Println("\033[94;1;4mThis State process finish, switch to next state.\033[0m")
	ps.context.currentState = &DealCardState{context: ps.context}
}

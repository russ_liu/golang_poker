package stateMachine

import (
	"fmt"
)

type GameRoundState struct {
	context *Context
}

func (gs *GameRoundState) preHandle() {
	fmt.Printf("\n\n\033[94;1;4mGame Round State...\033[0m\n")
	gs.context.roundNum++
	fmt.Printf("Round %d\n", gs.context.roundNum)
}

func (gs *GameRoundState) handle() {
	cycle := 0
	for {
		cycle++
		fmt.Printf("Cycle:%d\n", cycle)
		for _, player := range gs.context.players {
			fmt.Printf("%s's Pharse:\n", player.GetName())
			player.PlayCards()
			if player.GetPoker().GetLength() == 0 {
				fmt.Printf("\033[36mThis round winner is %s.\033[0m\n", player.GetName())
				gs.context.winnerRecords = append(gs.context.winnerRecords, player.GetName())
				player.PlusScore()
				return
			}
		}
	}
}

func (gs *GameRoundState) afterHandle() {
	//Next: DealCardState or SettlementStage
	fmt.Println("\033[94;1;4mThis State process finish, switch next to state.\033[0m")
	for _, player := range gs.context.players {
		if player.GetScore() >= gs.context.goalScore {
			gs.context.winner = player
			gs.context.currentState = &SettlementState{context: gs.context}
			return
		}
	}
	gs.context.currentState = &DealCardState{context: gs.context}
}

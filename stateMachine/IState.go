package stateMachine

type IState interface {
	preHandle()
	handle()
	afterHandle()
}

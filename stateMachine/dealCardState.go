package stateMachine

import (
	"fmt"
	"golangPoker/poker"
)

type DealCardState struct {
	context *Context
}

func (ds *DealCardState) preHandle() {
	fmt.Printf("\n\n\033[94;1;4mDeal Card State...\033[0m\n")
}

func (ds *DealCardState) handle() {
	fmt.Println("Set a new deck of cards...")
	ds.context.mainPokerBox.NewCards(ds.context.numOfDeck)
	fmt.Println("Shuffling cards...")
	ds.context.mainPokerBox.Shuffle()
	fmt.Printf(
		"Shuffled cards: %v\n\n",
		poker.FormatCards(ds.context.mainPokerBox.GetEncodeCards()))
	for _, player := range ds.context.players {
		dealCards := ds.context.mainPokerBox.DealCards(poker.CardNum * ds.context.numOfDeck / 4)
		fmt.Printf("Dealing cards to %s: %v\n", player.GetName(), poker.FormatCards(dealCards))
		player.RevCards(dealCards)
	}
}

func (ds *DealCardState) afterHandle() {
	fmt.Println("\033[94;1;4mThis State process finish, switch to next state.\033[0m")
	ds.context.currentState = &GameRoundState{context: ds.context}
}

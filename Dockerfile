FROM golang:1.17.2 AS builder

WORKDIR /
COPY . .

ENV GO111MODULE=on
RUN GOOS=linux GOARCH=amd64 CGO_ENABLED=0 go build -mod=vendor -o golangPoker

CMD ./golangPoker